// import {Component, OnInit} from '@angular/core';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
//
// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.scss']
// })
// export class RegisterComponent implements OnInit {
//   registerFormGroup: FormGroup;
//   ufc: FormControl;
//   pfc: FormControl;
//   cpfc: FormControl;
//
//   constructor() {
//   }
//
//   ngOnInit() {
//     // 1st param: initial value, 2nd param: validators
//     this.ufc = new FormControl('Test Initial Value', [Validators.email]);
//     this.pfc = new FormControl();
//     this.cpfc = new FormControl();
//     this.registerFormGroup = new FormGroup({
//       username: this.ufc,
//       password: this.pfc,
//       confirmPassword: this.cpfc
//     });
//     // valueChanges is Observable and is on formcontrol or formGroup
//     this.ufc.valueChanges
//       .subscribe((val) => {
//         console.log(val);
//       });
//   }
//
//   register() {
//     console.log(this.registerFormGroup);
//   }
// }
import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {debounceTime, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  static usernameExisted = false;
  registerFormGroup: FormGroup;
  count = 0;

  static validatePasswords(passwords: FormGroup): null | {} {
    // console.log(passwords);
    // ES6 destructure
    const {password: p, confirmPassword: cp} = passwords.value;
    return p === cp ? null : {passwordsNotMatch: 'Passwords has to match!'};
  }

  static validateUserExsited(username: FormControl): null | {} {
    return RegisterComponent.usernameExisted ? null : {usernameTaken: 'Username has already been taken.'};
  }


  // using FormBuilder to create a reactive form

  constructor(private fb: FormBuilder, public auth: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.registerFormGroup = this.fb.group(
      {
        username: ['', [Validators.required, Validators.minLength(3)]],
        passwords: this.fb.group({
          password: [''],
          confirmPassword: ''
        }, {validator: [RegisterComponent.validatePasswords]})
      }
    );
  }

  register() {
    console.log(this.registerFormGroup);
    const user = {username: this.registerFormGroup.value.username, password: this.registerFormGroup.value.passwords.password};
    this.auth.register(user).subscribe((value) => {
      console.log('Register Status:', value);
      this.router.navigate(['./login']);
    }, (err) => {
      console.log(err);
    });
  }

  usernameCheck($event) {
    // console.log($event);
    // console.log(this.registerFormGroup.controls.username);
    this.registerFormGroup.controls.username.valueChanges
      .pipe(debounceTime(500), switchMap((params) => {
        return this.auth.getAllUser();
      })).subscribe((users) => {
      RegisterComponent.usernameExisted = users.some((u) => {
        return u.username === $event.target.value;
      });
      this.auth.usernameTaken = RegisterComponent.usernameExisted ? 'exsited' : null;
      // console.log(RegisterComponent.usernameExisted, this.count++, this.auth.usernameTaken);
    });
  }
}

