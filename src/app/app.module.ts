import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductComponent} from './products/product.component';
import {ProductOverviewComponent} from './products/product-overview/product-overview.component';
import {HomeComponent} from './home/home.component';
import {HeaderComponent} from './header/header.component';
import {MyShowDirective} from './shared/directives/my-show.directive';
import {MyIfDirective} from './shared/directives/my-if.diective';
import {ColorAlertRedHelloDirective} from './shared/directives/color-alert-red-hello.directive';
import {HighlightDirective} from './shared/directives/highlight.directive';
import {ProductDetailComponent} from './products/product-detail/product-detail.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {PriceRangePipe} from './shared/pipes/price-range.pipe';
import {CurrencyExchangePipe} from './shared/pipes/currency-exchange.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CustomStyleModule} from './shared/modules/custom-style/custom-style.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ParentComponent } from './home/parent/parent.component';
import { ChildComponent } from './home/parent/child/child.component';
import {PrimeModule} from './prime/prime.module';
import { CakesComponent } from './cakes/cakes.component';
import { AddcakeComponent } from './cakes/addcake/addcake.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ResponsiveComponent } from './responsive/responsive.component';
import { FormDemoComponent } from './form-demo/form-demo.component';
import { TemplateFormComponent } from './form-demo/template-form/template-form.component';
import { ReactiveFormComponent } from './form-demo/reactive-form/reactive-form.component';
import {RouterModule} from '@angular/router';
import { VscodeComponent } from './vscode/vscode.component';



@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ProductOverviewComponent,
    HomeComponent,
    HeaderComponent,
    MyShowDirective,
    MyIfDirective,
    ColorAlertRedHelloDirective,
    HighlightDirective,
    ProductDetailComponent,
    PriceRangePipe,
    CurrencyExchangePipe,
    LoginComponent,
    RegisterComponent,
    ParentComponent,
    ChildComponent,
    CakesComponent,
    AddcakeComponent,
    AutocompleteComponent,
    ResponsiveComponent,
    FormDemoComponent,
    TemplateFormComponent,
    ReactiveFormComponent,
    VscodeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,   // two way binding
    HttpClientModule,  //angularjs $http service, angular 2\4 HTTPModule
    BrowserAnimationsModule,
    CustomStyleModule,
    PrimeModule,
    MatAutocompleteModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
