import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {debounceTime, map, startWith, switchMap} from 'rxjs/operators';
import {AutocompleteUsernameService} from '../shared/services/autocomplete-username.service';
import {User} from '../shared/models/user';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  options: User[];

  constructor(private as: AutocompleteUsernameService) {
  }

  ngOnInit(): void {
    // this.filteredOptions = this.myControl.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
  }

  // private _filter(value: string): User[] {
  //   const filterValue = value.toLowerCase();
  //   return this.as.getUsernameLike(value).subscribe()
  // }

  filterUsername() {
    this.myControl.valueChanges.pipe(debounceTime(100), switchMap(value => {
      return this.as.getUsernameLike(value);
    })).subscribe(value => {
      this.options = value;
    });
  }
}
