import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {debounceTime, switchMap} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userExisted: boolean;

  constructor(public auth: AuthService, private router: Router, private  location: Location) {
  }

  ngOnInit() {
  }

  submit(ngForm) {
    console.log('-------', ngForm);
    this.auth.login(ngForm.value)
      .subscribe((value) => {
        console.log(value);
        if (value.success) {
          // this.auth.user = {username: 'bob'};
          this.auth.user = value.user;
          // this.router.navigate().catch();
          this.router.navigate(['/home']);
        }
      }, (err) => {
        console.log(err);
      });
  }

  userCheck(ngForm) {
    console.log(ngForm);
    ngForm.controls.username.valueChanges.pipe(debounceTime(500), switchMap((params) => {
      return this.auth.getAllUser();
    })).subscribe((users) => {
      const result = users.some((u) => {
        return u.username === ngForm.value.username;
      });
      this.auth.usernameTaken = result ? 'exsited' : null;
      console.log(this.userExisted);
    });
  }
}
