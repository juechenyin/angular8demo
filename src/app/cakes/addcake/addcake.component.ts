import { Component, OnInit } from '@angular/core';
import {CakeserviceService} from '../../shared/services/cakeservice.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-addcake',
  templateUrl: './addcake.component.html',
  styleUrls: ['./addcake.component.scss']
})
export class AddcakeComponent implements OnInit {

  constructor(private cs: CakeserviceService, private router: Router) { }

  ngOnInit() {
  }

  submit(ngForm) {
    console.log(ngForm.value);
    this.cs.addCake(ngForm.value);
    this.router.navigate(['/cakes']);
  }
}
