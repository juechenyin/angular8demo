import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcakeComponent } from './addcake.component';
import {Router, RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';

describe('AddcakeComponent', () => {
  let component: AddcakeComponent;
  let fixture: ComponentFixture<AddcakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterModule, RouterTestingModule, FormsModule],
      declarations: [ AddcakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
