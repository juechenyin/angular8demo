import { Component, OnInit } from '@angular/core';
import {Cake} from '../shared/models/cake';
import {CakeserviceService} from '../shared/services/cakeservice.service';

@Component({
  selector: 'app-cakes',
  templateUrl: './cakes.component.html',
  styleUrls: ['./cakes.component.scss']
})
export class CakesComponent implements OnInit {
  cakes: Cake[];

  constructor(private cs: CakeserviceService) { }

  ngOnInit() {
    this.cakes = this.cs.getCakes();
  }

}
