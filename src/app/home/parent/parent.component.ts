import {
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  ContentChild,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit, QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {ChildComponent} from './child/child.component';

/*
* LifeCycle - component/directive HookMethod
*        // for component and directive
        constructor
        ngOnInit - data initialization
        ngOnDestroy - before component will be destroyed.e.g. navigate away.
        ngOnChanges - will be invoked whenever @input data binding changes(pure change)
        ngDoCheck - invoked when parent change detector runs
        // for component only
        ngAfterViewInit - invoked after child/DOM components ready
        ngAfterViewChecked
        ngAfterContentInit - invoked after contents passed from parents done initialization
        ngAfterContentChecked
* */
@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit, OnDestroy, OnChanges, DoCheck, AfterViewInit, AfterViewChecked, AfterContentInit, AfterViewChecked {

  parentData: string;
  @ViewChild(ChildComponent, {static: true}) childComponent;
  // @ViewChildren()
  @ViewChild('h2', {static: true}) h2;
  @ViewChild('cmp') cmp;
  @ViewChildren(ChildComponent) list;
  // @ContentChild();     get from ngContent
  @ContentChild(ChildComponent) cc;
  constructor() {
  }

  ngOnInit() {
    console.log(this.childComponent);
    console.log(this.h2);
    console.log('OnInit');
  }

  setData($event) {
    console.log($event);
    this.parentData = $event;
  }

  ngOnDestroy(): void {
    console.log('OnDestroy');
  }

  ngOnChanges(): void {
    console.log('Onchanges');
  }

  ngAfterContentInit(): void {
    console.log(this.cc)
    console.log('AfterContentInit');
  }

  ngAfterViewChecked(): void {
    console.log('AfterViewChecked');
  }

  ngAfterViewInit(): void {
    console.log(this.cmp.nativeElement.innerHTML);
    console.log(this.list);
    console.log('AfterViewInit');
  }

  ngDoCheck(): void {
    console.log('After Do Check');
  }
  // OnInit
  // parent.component.ts:78 After Do Check
  // parent.component.ts:66 AfterContentInit
  // parent.component.ts:74 AfterViewInit
  // parent.component.ts:70 AfterViewChecked
  // parent.component.ts:78 After Do Check
  // parent.component.ts:70 AfterViewChecked
  // parent.component.ts:78 After Do Check
  // parent.component.ts:70 AfterViewChecked
  // On destory
}
