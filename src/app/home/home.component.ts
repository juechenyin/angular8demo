import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../shared/services/products.service';
import {Product} from '../shared/models/product';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  // product = {
  //   name: 'iPhone6',
  //   brand: 'Apple',
  //   price: 900,
  //   stock: 111,
  //   image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone6.jpg'
  // };
  product: Product;
  myClass: string;
  flag: boolean;
  // time: Observable<string> = new Observable<string>(value => {
  //   value.next( v => console.log(new Date().toString()));
  // });

  constructor(private ps: ProductsService) {
  }

  ngOnInit() {
    // this.ps.getProduct(4).then((p: Product) => {
    //   this.product = p;
    // }).catch();
    // observable is lazy, if you don't subscribe, it will not executed.
    this.ps.getProduct(4).subscribe((product: Product) => {
      this.product = product;
    }, () => {

    }, () => {

    });
    // this.ps.getProducts().subscribe((products: Product[]) => {
    //     this.products = products;
    //   }, () => {
    //
    //   }, () => {
    //     console.log('Request observed is completed');
    //   }
    // );
  }
}
