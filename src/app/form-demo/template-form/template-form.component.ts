import { Component, OnInit } from '@angular/core';
import {map, switchMap} from 'rxjs/internal/operators';
import {AutocompleteUsernameService} from '../../shared/services/autocomplete-username.service';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss']
})
export class TemplateFormComponent implements OnInit {

  constructor(private as: AutocompleteUsernameService) { }

  ngOnInit(): void {
  }

  submit(ngForm){
    console.log(ngForm);
  }
  usernameInput(ngForm){
    // console.log(ngForm);
    ngForm.controls.username.valueChanges.pipe(switchMap(value => {return this.as.getUsernameLike(value + '')})).subscribe(value => console.log(value));
  }
  genderInput(ngControl){
    console.log(ngControl);
  }
}
