import {Component, Input} from '@angular/core';
import {CurrencyService} from '../../shared/services/currency.service';

@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.scss']
})
export class ProductOverviewComponent {
  @Input() product;
  @Input() item: string;

  // set to public, then you can access this variable in your html
  constructor(public cs: CurrencyService) {
  }
}
