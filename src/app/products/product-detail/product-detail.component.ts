import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from '../../shared/services/products.service';
import {Product} from '../../shared/models/product';
import {switchMap} from 'rxjs/operators';
import {CurrencyService} from '../../shared/services/currency.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.conponent.scss']
})

export class ProductDetailComponent implements OnInit {
  product: Product;

  constructor(private ar: ActivatedRoute, private ps: ProductsService, private router: Router, public cs: CurrencyService) {
  }

  ngOnInit(): void {
    // console.log(this.ar);
    // console.log(this.ar.paramMap);
    // const id = this.ar.snapshot.params.id;
    // this.product = this.ps.getProduct(id);
    this.ar.paramMap
      .pipe(switchMap((params) => {
        const id = +params.get('id'); // +: transform string to a number
        return this.ps.getProduct(id); // return an observable
      })).subscribe((product: Product) => {
      this.product = product;
    }, (errMsg: string) => {
        console.log('errMsg: ', errMsg);
    });
    // console.log(this.ar);
    // this.ar.paramMap.subscribe((params) => {
    //   const id = +params.get('id');
    //   return this.ps.getProduct(id).subscribe((product: Product) => {
    //     this.product = product;
    //   });
    // });


    //
    //
    //   .subscribe(params => {
    //   const id = +params.get('id'); //+ transform string to a number
    //   this.ps.getProduct(id).subscribe((product: Product) => {
    //     this.product = product;
    //   }, () => {
    //
    //   }, () => {
    //
    //   });
    // });
    // // this.ps.getProduct(id).then((p: Product) => {
    // //   this.product = p;
    // // }).catch();

  }

  goToNext() {
    console.log(this.router);
    let id = this.ar.snapshot.params.id;
    this.router.navigate(['/product-detail', ++id]).catch(); // /product-detail, product-detail/, product-detail seems no difference?.    /: find in current path. ./ find from previous
  }

  goPrevious() {
    let id = this.ar.snapshot.params.id;
    this.router.navigate(['/product-detail', --id]).catch();
  }
}
