import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProductsService} from '../shared/services/products.service';
import {Product} from '../shared/models/product';
import {CurrencyService} from '../shared/services/currency.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  //VewEncapsulation: default is emulated
  // encapsulation: ViewEncapsulation.Emulated,
  // encapsulation: ViewEncapsulation.None
  // encapsulation: ViewEncapsulation.ShadowDom
})

// lif cycle hook methods
export class ProductComponent implements OnInit {
  // products = [
  //   { name: 'iPhone', brand: 'Apple', price: 100, stock: 22, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone.jpg' },
  //   { name: 'iPhone3G', brand: 'Apple', price: 200, stock: 33, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone3G.jpg'},
  //   { name: 'iPhone3GS', brand: 'Apple', price: 300, stock: 11, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone3GS.jpg'},
  //   { name: 'iPhone4', brand: 'Apple', price: 400, stock: 22, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone4.jpg'},
  //   { name: 'iPhone4S', brand: 'Apple', price: 500, stock: 33, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone4S.jpg'},
  //   { name: 'iPhone5', brand: 'Apple', price: 600, stock: 11, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone5.jpeg'},
  //   { name: 'iPhone5C', brand: 'Apple', price: 700, stock: 222, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone5c.png'},
  //   { name: 'iPhone5S', brand: 'Apple', price: 800, stock: 333, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone5s.jpg'},
  //   { name: 'iPhone6', brand: 'Apple', price: 900, stock: 111, image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone6.jpg'}
  // ];
  products: Product[];
  parentMsg: string;
  min: number;
  max: number;
  // ps;
  // // one purpose in component is to do dependency injection
  // constructor(ps: ProductsService) {
  //   this.ps = ps;
  // }
  constructor(private ps: ProductsService, public cs: CurrencyService) {
  }

  ngOnInit() {// only invoke once
    // this.ps.getProducts()
    //   .then((products: Product[]) => {
    //     this.products = products;
    //   })
    //   .catch();
    // this.parentMsg = 'THIS MSG IS FROM PARENT';
    this.ps.getProducts().subscribe((products: Product[]) => {
        console.log('Request observed in next');
        this.products = products;
      }, () => {

      }, () => {
        console.log('Request observed is completed');
      }
    );
  }

  addAProduct() {
    console.log('add a product.');
    // this.products = this.products.slice();
    // this.products = [...this.products];
    // this.products.push({
    //   name: 'iPhone11',
    //   brand: 'Apple',
    //   price: 1900,
    //   stock: 111,
    //   image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone6.jpg'
    // });
    this.products = [
      ...this.products, {
        name: 'iPhone11',
        brand: 'Apple',
        price: 1900,
        stock: 111,
        image: 'https://s3.us-east-2.amazonaws.com/msi-tech-2019/iphone6.jpg'
      }
    ];
  }

  updateCurrencyCode($event) {
    this.cs.currencyCode = $event.target.value;
  }
}
