import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ProductComponent} from './products/product.component';
import {ProductDetailComponent} from './products/product-detail/product-detail.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuard} from './auth.guard';
import {CakesComponent} from './cakes/cakes.component';
import {AddcakeComponent} from './cakes/addcake/addcake.component';
import {ResponsiveComponent} from './responsive/responsive.component';
import {FormDemoComponent} from './form-demo/form-demo.component';


const routes: Routes = [
    {
      path: 'home',
      component: HomeComponent
    },
    {
      path: 'responsive',
      component: ResponsiveComponent
    },
    {
      path: 'products',
      component: ProductComponent
    },
    {
      path: 'product-detail/:id',
      component: ProductDetailComponent
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'cakes',
      component: CakesComponent
    },
    {
      path: 'add-cake',
      component: AddcakeComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'prime',
      canLoad: [AuthGuard],
      // before angular8, if you want to lazy loading a module:
      // loadChildren: './prime/prime.module#PrimeModule',
      // Angular 8 lazy loading syntax
      loadChildren: () => import('./prime/prime.module').then(m => m.PrimeModule)
    },
    {
      path: 'form-demo',
      component: FormDemoComponent
    },
    {
      // match everything, always put this in the end
      path: '**',
      redirectTo: 'home'
    }
  ]
;

@NgModule({
  // one application should only have one forRoot
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
