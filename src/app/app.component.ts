import {Component} from '@angular/core';

// @ means decorator: decorator pattern
@Component({
  // metadata
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng8demo';
  link = 'https://www.google.com';
  onClick = () => {
    this.title = 'NEW TITLE';
  }
  updateTitle = ($event) => {
    this.title = $event.target.value;
  }
}
