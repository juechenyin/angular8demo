export class Cake {
  name: string;
  size: number;
  cream: boolean;
  price: number;
}
