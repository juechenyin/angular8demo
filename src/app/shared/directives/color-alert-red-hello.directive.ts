import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appColorAlertRedHello]'
})

export class ColorAlertRedHelloDirective {
  constructor() {
  }

  @HostBinding('style.color') color = 'red';

  @HostListener('click', ['$event']) onClick($event) {
    alert('Hello' + $event.target.outerHTML);
  }
  @HostBinding('style.height') height = '150px';
}
