import {Pipe, PipeTransform} from '@angular/core';
import {Product} from '../models/product';

@Pipe({
  name: 'priceRange',
  // pure: false // false means impure pipe
})

export class PriceRangePipe implements PipeTransform {
  transform(products: Product[], min: number, max: number): Product[] { // ...rest parameters. it will take unknown numbers and put it into an array
   // not quite good to modify the params
    min = min || Number.MIN_VALUE;
    max = max || Number.MAX_VALUE;
    return products.filter((p: Product) => {
      return p.price >= min && p.price <= max;
    });
  }

}
