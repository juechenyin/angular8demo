import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'currencyExchange'
})

export class CurrencyExchangePipe implements PipeTransform {
  RATES = {
    USD: 1,
    CNY: 7.04,
    JPY: 108.52
  };

  transform(price: number, currencyCode: string): number {
    return price * this.RATES[currencyCode];
  }
}
