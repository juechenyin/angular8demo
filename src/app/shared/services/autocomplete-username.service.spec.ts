import { TestBed } from '@angular/core/testing';

import { AutocompleteUsernameService } from './autocomplete-username.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AutocompleteUsernameService', () => {
  let service: AutocompleteUsernameService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule]
    });
    service = TestBed.inject(AutocompleteUsernameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
