import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutocompleteUsernameService {

  constructor(private httpClient: HttpClient) { }

  getUsernameLike(username: string): Observable<User[]>{
    return this.httpClient.get<User[]>(`${environment.API_URL}/users/${username}`);
  }
}
