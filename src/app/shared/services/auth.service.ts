import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = null;
  usernameTaken = null;
  constructor(private httpeClient: HttpClient) {
    this.checkLogin().subscribe((value: { success: boolean, user: any }) => {
      // if (value.success) {
      //   this.user = value.user;
      // } else {
      //   this.user = null;
      // }
      // value: success ? this.user = value : user;
      // this.user = null;
      this.user = value.success ? value.user : null;
    });
  }

  login(user): Observable<{ success: boolean, user: any }> {
    // spring security only supports FormUrlEncoded data(form data)
    // username=admin&password=adminpass ---- form data
    const userParam = new HttpParams().append('username', user.username).append('password', user.password);
    return this.httpeClient.post<{ success: boolean, user: any }>(
      `${environment.API_URL}/login`, userParam, {withCredentials: true}); // withCredential is to set cookies
  }

  logout() {
    return this.httpeClient.get(`${environment.API_URL}/logout`, {withCredentials: true});
  }

  checkLogin() {
    return this.httpeClient.get(`${environment.API_URL}/checklogin`, {withCredentials: true});
  }

  register(user): Observable<{ success: boolean }> {
    const userParam = new HttpParams().append('username', user.name).append('password', user.password);
    // console.log(userParam);
    // console.log(JSON.stringify(userParam));
    return this.httpeClient.post<{ success: boolean }>(`${environment.API_URL}/myusers`, user);
  }

  getAllUser(): Observable<User[]> {
    return this.httpeClient.get<User[]>(`${environment.API_URL}/users`);
  }
}
