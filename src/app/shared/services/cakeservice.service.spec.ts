import { TestBed } from '@angular/core/testing';

import { CakeserviceService } from './cakeservice.service';

describe('CakeserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CakeserviceService = TestBed.get(CakeserviceService);
    expect(service).toBeTruthy();
  });
});
