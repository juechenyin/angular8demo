import {Injectable} from '@angular/core';
import {Cake} from '../models/cake';

@Injectable({
  providedIn: 'root'
})
export class CakeserviceService {
  cakes: Cake[] = [];

  constructor() {
  }

  addCake(cake) {
    this.cakes.push(cake);
  }

  getCakes() {
    return this.cakes;
  }
}
