import {TestBed, async, inject} from '@angular/core/testing';

import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ProductsService} from './products.service';
import {Product} from '../models/product';
import {defer} from 'rxjs';

function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}


describe('ProductService', () => {
  let httpClientSpy;
  let myProductsService: ProductsService;
  const expectedProducts: Product[] = [
    {id: 1, name: 'test1', brand: 'apple', price: 100, stock: 100, image: ''},
    {id: 2, name: 'test2', brand: 'google', price: 200, stock: 100, image: ''},
    {id: 3, name: 'test3', brand: 'amazon', price: 300, stock: 100, image: ''}
  ];
  const expectedProduct: Product = {id: 3, name: 'test3', brand: 'amazon', price: 300, stock: 100, image: ''};
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    myProductsService = new ProductsService(httpClientSpy);
  });

  it('should be created', () => {
    expect(myProductsService).toBeTruthy();
  });

  it('should get products', (done) => {
    httpClientSpy.get.and.returnValue(asyncData(expectedProducts));
    myProductsService.getProducts().subscribe((products) => {
      expect(products).toEqual(expectedProducts);
      done();
    });
  });

  it('should get a product', (done) => {
    httpClientSpy.get.and.returnValue(asyncData(expectedProduct));
    myProductsService.getProduct(3).subscribe((product) => {
      expect(product).toEqual(expectedProduct);
      done();
    });
  });

});
