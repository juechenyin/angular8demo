import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  constructor(public auth: AuthService, private router: Router) {
  };

  title = 'MSI-ECommerce';

  logout() {
    this.auth.logout()
      .subscribe((value: { success: boolean }) => {
        if (value.success) {
          this.auth.user = null;
          this.router.navigate(['/login']).catch();
        }
      });
  }

  ngOnInit(): void {
    // setTimeout(() => {
    //   this.title = 'TTTTTTTTTTT';
    // }, 5000);
  }
}
